# N.E.E.T team presents "dota 3" game
### Best game ever made at the CEPU in freshman year

###### Lydesi's development team 2k19
* Members
    * Seydametov Seydali (Team lead, free-hands)
    * Memetov Mansur (Unity GameDev)
    * Adshigaparov Vilen (Scratch GameDev)
    * Nesterenko Sergey (Main game designer)
    * Zhivotovskaya Anastasia (Level design helper, free-hands)

![Unity image](https://unity3d.com/files/images/ogimg.jpg)
![Scratch image](https://scratch.mit.edu/images/scratch-og.png)
